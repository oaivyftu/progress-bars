import React from 'react';
import ReactDOM from 'react-dom';
import { toast } from 'react-toastify';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';
import 'styles/styles.scss';
import 'react-toastify/dist/ReactToastify.css';

// Call it once in your app. At the root of your app is the best place
toast.configure();

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
