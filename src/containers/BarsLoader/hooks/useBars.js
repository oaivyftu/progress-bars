import { useReducer, useState, useEffect } from 'react';
import service from 'utils/service';
import { API_BARS } from 'utils/constants';
import { toast } from 'react-toastify';
import produce from 'immer';
import { CHANGE_PROGRESS, CHANGE_BAR, UPDATE_DATA } from '../constants';

export const barsReducer = (state, action) => {
  switch (action.type) {
    case UPDATE_DATA:
      // eslint-disable-next-line no-unused-vars
      return {
        ...action.data,
        bars: (action.data.bars || []).reduce(
          (prev, curVal, curIdx) => ({
            ...prev,
            [curIdx]: curVal,
          }),
          {},
        ),
        active: 0,
      };

    case CHANGE_PROGRESS:
      return produce(state, draft => {
        draft.bars[draft.active] = Math.max(
          parseInt(state.bars[state.active], 10) + parseInt(action.val, 10),
          0,
        );
      });

    case CHANGE_BAR:
      return produce(state, draft => {
        draft.active = action.index;
      });

    default:
      throw new Error('Unknown action type');
  }
};

export default () => {
  const [barState, dispatch] = useReducer(barsReducer, null);

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);

        const response = await service.get(API_BARS);

        if (response.status === 200) {
          dispatch({
            type: UPDATE_DATA,
            data: response.data,
          });
        }
      } catch (err) {
        toast(err.message);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  return {
    barState,
    loading,
    dispatch,
  };
};
