import React, { useMemo } from 'react';
import {
  Container,
  Row,
  Col,
  Button as PreButton,
  Input,
  FormGroup,
  Spinner,
  Progress as PreProgress,
} from 'reactstrap';
import PropTypes from 'prop-types';
import { useBars } from './hooks';
import './index.scss';
import { CHANGE_PROGRESS, CHANGE_BAR } from './constants';

const Button = ({ btn, ...props }) => (
  <PreButton color="primary" key={`btn-control-${btn}`} {...props}>
    {btn > 0 && '+'}
    {btn}
  </PreButton>
);

Button.propTypes = {
  btn: PropTypes.number.isRequired,
};

const Progress = ({ key, bar, limit, ...props }) => (
  <PreProgress
    key={key}
    value={(bar * 100) / limit}
    color={bar / limit > 1 ? 'danger' : 'primary'}
    {...props}
  >
    {Math.ceil((bar * 100) / limit)}%
  </PreProgress>
);

Progress.propTypes = {
  key: PropTypes.oneOfType([
    PropTypes.number.isRequired,
    PropTypes.string.isRequired,
  ]),
  bar: PropTypes.number.isRequired,
  limit: PropTypes.number.isRequired,
};

export default () => {
  const { barState, dispatch, loading } = useBars();

  const { bars, buttons, limit, active } = barState || {};

  const renderBars = useMemo(
    () =>
      Object.entries(bars || []).map(([key, bar]) => (
        <Progress key={key} bar={bar} limit={limit} />
      )),
    [bars],
  );

  const renderButtons = useMemo(
    () =>
      (buttons || []).map(btn => (
        <Button
          btn={btn}
          key={btn}
          onClick={() =>
            dispatch({
              type: CHANGE_PROGRESS,
              val: btn,
            })
          }
        />
      )),
    [buttons],
  );

  const renderOptions = useMemo(
    () =>
      Object.entries(bars || []).map(([key]) => (
        <option key={key} value={key}>
          Progress {parseInt(key, 10) + 1}
        </option>
      )),
    [bars],
  );

  if (loading) {
    return (
      <Container className="bars-loader text-center">
        <Spinner color="primary" />
      </Container>
    );
  }

  if (!barState) {
    return (
      <Container className="bars-loader">
        <p>No data</p>
      </Container>
    );
  }

  return (
    <Container className="bars-loader">
      <Row>
        <Col xs="12">
          <h1>Progress Bars</h1>
          {renderBars}
        </Col>
      </Row>
      <Row>
        <Col xs="12" md="3">
          <FormGroup>
            <Input
              type="select"
              value={active}
              name="select-progress"
              onChange={e =>
                dispatch({
                  type: CHANGE_BAR,
                  index: e.target.value,
                })
              }
            >
              {renderOptions}
            </Input>
          </FormGroup>
        </Col>
        <Col xs="12" md="9">
          {renderButtons}
        </Col>
      </Row>
    </Container>
  );
};
