import axios from 'axios';
import { API_HOST } from './constants';

const service = axios.create({
  baseURL: API_HOST,
});

export default service;
